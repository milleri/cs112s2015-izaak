import java.util.Scanner;
import java.util.Stack;

/**@Izaak Miller**/
public class StackMachine
{
    public static void main(String args[])
    {
        /** Creates new Stack, String and Scanner.*/
        Stack<String> Stack = new Stack();
        Scanner scan = new Scanner(System.in);
        String input = "blank";

        /**Instructions of StackMachine*/
        System.out.println("Welcome to the Stack Machine");
        System.out.println("Enter an integer, 's' or '+' to add to the stack");
        System.out.println("Enter 'd' to display the contents of the stack");
        System.out.println("Enter 'x' to stop the Stack Machine");
        System.out.println("Enter 'e' to evaluate the stack");

        /**Saves command as input to be checked in if statements*/
        input = scan.next();

        /**StackMachine runs until user enters 'x' command*/
        while(!input.equals("x"))
        {
            if (input.equals("x"))
            {
                break; /**Breaks when user enters 'x' command*/
            }

            /**Displays the stack to the user*/
            else if (input.equals("d"))
            {
                if(Stack.empty() == true)
                {
                    System.out.println("Stack is empty");
                }
                else
                {
                    System.out.println("Stack: " +Stack);
                }
            }

            /**Evaluates the stack. If top of stack contains 's', then the next two variables in the
             * stack will are switch. If the top of stack contains '+', then the next two integers
             * are added together and pushed back into stack. If there is nothing in the stack,
             * then that is stated*/
            else if (input.equals("e"))
            {
            if(Stack.empty() == true)
            {
                System.out.println("Stack is empty");
            }

            else if (Stack.peek().equals("s"))
            {
                Stack.pop();
                String a = Stack.pop();
                String b = Stack.pop();
                Stack.push(a);
                Stack.push(b);

                System.out.println("Stack: " +Stack);
            }
            else if (Stack.peek().equals("+"))
            {
                Stack.pop();
                int c = Integer.parseInt(Stack.pop());
                int d = Integer.parseInt(Stack.pop());

                int sum = c + d;

                Stack.push(Integer.toString(sum));
                System.out.println("Stack: " +Stack);
            }
            else
            {
                System.out.println("Stack: " +Stack);
            }
            }

            /**If none of the commands are entered, then the value is pushed to the top of the stack*/
            else
            {
                Stack.push(input);
            }

            /**Asks user to enter another command to avoid infinite loop.*/
            System.out.println("Enter another command");
            input = scan.next();
        }
    }
}
