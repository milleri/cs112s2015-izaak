public class Hooray {

    public static void hooray() {
        while(true) // infinite while loop that never stops unless the user inputs control c
            //it never stops running because it is always true
            System.out.println("Hooray!"); // prints hooray
        }
    }

    public static void main(String[] args) {
        hooray(); //calls hooray method
    }

}
