public class Weeee {

    public static void weeee() { //weee method that prints out weee over and over
        System.out.println("Weeee!"); // the program runs until an error message is displayed
        weeee(); // error
    }

    public static void main(String[] args) {
        weeee(); // calls weee method
    }

}
