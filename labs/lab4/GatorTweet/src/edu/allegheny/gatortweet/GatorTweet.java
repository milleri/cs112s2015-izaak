package edu.allegheny.gatortweet;

import java.util.Scanner;
import java.util.ArrayList;
import java.io.*;
import java.io.FileNotFoundException;


import twitter4j.*;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

import java.util.List;


/**
 * @author Izaak Miller <milleri@allegheny.edu>
 * @since @1-29-2015
 */

public class GatorTweet
{
    public static void main(String[] args)

     {
     File TweetList = new File ("tweet/Tweets.txt"); /** Makes the tweet.txt and object */
     ArrayList<Tweet> ValidTweets = new ArrayList<Tweet>(); /** Creates array list for valid tweets*/
     ArrayList<String> InvalidTweets = new ArrayList<String>(); /** Creates array list for invalid tweets*/
    // String input = new String(args[0]); /**takes argument given by user and stores it*/
 Twitter twitter = new TwitterFactory().getInstance(); /** gets Twitter instance with default credentials*/



        try /** Try catch instead of throws; Have to catch the FileNotFoundException error */
            {
             Scanner scan = new Scanner(TweetList); /** Reading throw text file where tweets are */

                while (scan.hasNextLine()) /** Loop reads text file until there are no more tweets */
            {
                String newTweet = scan.nextLine();
                Tweet tw = new Tweet(); /** Creates an instance of the tweet */


                if (tw.isValidMessage(newTweet) == true) /** Read text file and calls method to see if the tweet is valid */
                {
                    tw.setMessage(newTweet); /** Sets the message */
                    ValidTweets.add(tw); /** Adds valid tweet to the corresponding array list */
                    //Status status = twitter.updateStatus(newTweet);/** Adds valid tweets to my status*/
                }
                else
                {
                    tw.setMessage(newTweet);
                    InvalidTweets.add(newTweet); /** Adds invalid tweet to the corresponding array list */
                }

            }
                     }catch (FileNotFoundException e)
            {
                System.out.println("An error has occurred");
            }
        Scanner scan = new Scanner(System.in); /** New Scanner to read user input*/
        System.out.println("What would you like to do? GetTimeline, UpdateStatus or send a DirectMessage?");
        String input = scan.next(); /** saves response into input, which is later used to decide action in program*/

    if (input.equalsIgnoreCase("GetTimeline")) /** Updates Timeline and displays it in terminal */
     {
        try
            {
            ConfigurationBuilder cb = new ConfigurationBuilder(); /***/
            cb.setDebugEnabled(true)
                .setOAuthConsumerKey("rPtRCCRqdDyoxHS3E2UARA")
                .setOAuthConsumerSecret("hhDnR4NETStvN4F84km2xuBy3eXJ8l2FnjdL23YPs");
            TwitterFactory tf = new TwitterFactory(cb.build());
            User user = twitter.verifyCredentials(); /** verifies user*/
            List<Status> statuses = twitter.getHomeTimeline();
            System.out.println("Showing @" + user.getScreenName() + "'s home timeline."); /** Labels each status with corresponding user*/
            for (Status status : statuses)
                {
                System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText());
                }
            } catch (TwitterException te) /** Displays error message*/
                {
                te.printStackTrace();
                System.out.println("Failed to get timeline: " + te.getMessage());
                System.exit(-1);
                }
     }
        if (input.equalsIgnoreCase("DirectMessage")) /** Allows direct message to be sent if user inputs DirectMessage */
        {
                try
                {
                    System.out.println("Who would you like to send a message to? (@)");
                    String sendTo = scan.next(); /** saves username that message will be sent to*/
                    System.out.println("Please enter a message to send:");
                    String nothing = scan.nextLine(); /** scans message once into empty string*/
                    String dmessage = scan.nextLine(); /** saves message to be sent to user*/
                    DirectMessage DirectMessage = twitter.sendDirectMessage(sendTo,dmessage); /** sends message taking in user name and message */

                }catch (TwitterException te) /** catch error and prints out error message */
                {
                    te.printStackTrace();
                    System.out.println("Failed to send a direct message: " + te.getMessage());
                    System.exit(-1);
                }
            }
        if (input.equalsIgnoreCase("UpdateStatus")) /** Allows user to update status */
        {
            try
            {
                System.out.println("What would you like your status to be? (Limit 140 characters)");
                String blank = scan.nextLine(); /** empty string*/
                String UpdateStatus = scan.nextLine(); /** Saves new status*/
                /** Updates status taking in 140 characters. */
            }catch (TwitterException te) /** Prints error statement */
            {
                System.out.println("Failed to update status.");
            }
        }
            System.out.println("Finished");
    }


}
