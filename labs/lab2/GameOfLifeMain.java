//************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Izaak Miller
// CMPSC 111 Fall 2014
// Final project
// Date: December 5, 2014
//
// Purpose: To recreate the game of life
//************************************
import java.util.Date; // Prints today's date
import java.util.Scanner; // allows for user input
import java.util.Random; // random number generator
import java.util.Iterator; // interator that moves through the boardgame spots

public class  GameOfLifeMain
{
	//main method: program execution starts here
	public static void main(String[] args)
	{

        //variables
        String answer, setHouse, house;
        int job;
        double account,setSalary, addPayday;
        int position1 = 0;
        int position2 = 0;
        int currentPosition;
        int[] board = new int[60];

        int houseCheck = 0;



        //instance of players
        Player p1 = new Player (0.0, 20000.0, "none");
        Player p2 = new Player (0.0, 20000.0, "none");

        Player career = new Player(0.0, 20000.0, "none");

		System.out.println("Izaak Miller\nFinal Project\n" + new Date() + "\n");

        Scanner scan = new Scanner(System.in);
        Spinner spin = new Spinner();

        System.out.println("Welcome to the game of Life! \nThis is a two player game and each player will start with $20,000.");


        System.out.println("Player one, please choose a number between 1-5 to decide what your job will be.");
        job = scan.nextInt();
        setSalary = career.PlayerJob(job);
        p1.salary(setSalary);
        p1.getSalary();
        System.out.printf("You will be making $%.2f every time you land on payday\n", p1.getSalary());

        System.out.println("Player two, please choose a number between 1-5 to decide what your job will be.");
        job = scan.nextInt();
        setSalary = career.PlayerJob(job);
        p2.salary(setSalary);
        p2.getSalary();
        System.out.printf("You will be making $%.2f every time you land on payday\n", p2.getSalary());


        // blank space = 0 payday = 1, buy house = 2
        for (int i = 0; i <= 59; i++)
        {
            if (i%2 == 0)
                board[i] = 1;
            else
                board[i] = 0;
        }


        System.out.println("Player one goes first");
        while (position1 < 50 || position2 < 50)
        {
            int nextCount = 0;
            while(nextCount<1)
            {
            System.out.println("Player one, are you ready to spin or would you like to check your money and the space you are on? (Spin or Check)");
            answer = scan.next();
            if (answer.equalsIgnoreCase("Spin"))
            {
                currentPosition = spin.Spin();
                System.out.println("You spun a " + currentPosition + ".\n");
                position1 += currentPosition;
                nextCount++;
                if(position1!=0)
                {
                    if(board[position1]==1)
                    {
                        p1.addPayday(p1.getSalary());
                        System.out.printf("You landed on a payday! $%.2f will be added to your account. You now have $%.2f!\n\n",p1.getSalary(),p1.getAccount());
                    }
                    if(position1 >= 25)
                    {
                        while(houseCheck == 0)
                        {
                            System.out.println("Woah, stop! It is time to choose a house! Which house would you like: ");
                            System.out.println("A trailer for $20,000, a condo for $30,000, a cottage for $50,000, a lakehouse for $65,000 or a mansion for $90,000?");
                            house = scan.next();
                            setHouse = p1.chooseHouse(house);
                            p1.house(setHouse);
                            System.out.println("You own a " + p1.getHouse());
                            houseCheck++;
                        }
                    }
                }
            }
            else
                System.out.printf("You are %d spaces away from the start and you have $%.2f\n",position1, p1.getAccount());
            }

            while(nextCount<2)
            {
            System.out.println("Player two, are you ready to spin or would you like to check your money and the space you are on? (Spin or Check)");
            answer = scan.next();
            if (answer.equalsIgnoreCase("Spin"))
            {
                currentPosition = spin.Spin();
                System.out.println("You spun a " + currentPosition + ".\n");
                position2 += currentPosition;
                nextCount++;
                if(board[position1]!=0)
                {
                    if(board[position1]==1)
                    {
                        p2.addPayday(p2.getSalary());
                    }
                    if(position1>=50)
                    {
                        while(houseCheck ==0)
                        {
                            System.out.println("Woah, stop! It is time to choose a house! Which house would you like: ");
                            System.out.println("A trailer for $20,000, a condo for $30,000, a cottage for $50,000, a lakehouse for $65,000 or a mansion for $90,000?");
                            house = scan.next();
                            setHouse = p2.chooseHouse(house);
                            p2.house(setHouse);
                            System.out.println("You own a " + p2.getHouse());
                            houseCheck++;

                        }
                    }
                }
            }
            else
                System.out.printf("You are %d spaces away from the start and you have $%.2f\n",position2, p2.getAccount());
            }

            nextCount = 0;

        }
        System.out.println("Stop! The game of life is over! Let's see how each player did.");
        System.out.printf("Player one had $%.2f and owned a %s!\n\n",p1.getAccount(),p1.getHouse());
        System.out.printf("Player two had $%.2f and owned a %s!", p2.getAccount(), p1.getHouse());

    }

}
