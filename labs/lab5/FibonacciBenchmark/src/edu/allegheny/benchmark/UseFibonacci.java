package edu.allegheny.benchmark;

import com.clarkware.Profiler;
import java.io.*;

public class UseFibonacci {

    public static void main(String[] args) {

        System.out.println("Begin experiment with different Fibonacci " +
                "implementations ..."); System.out.println();

        // extract the value that was passed on the command line; this is the
        // nth fibonacci number that we must calculate in the three different
        // fashions
        Integer Num = new Integer(args[0]); int num = Num.intValue();

        // determine which algorithm we are supposed to benchmark
        String chosen = args[1];
        String secondArg = args[2];


        if( chosen.equals("iterative") && secondArg.equals("int") ) // argument that accepts iterative & int
        {

            Profiler.begin("IterativeFibonacciInt"); //begins program for iterative Fib
            int iterativeFib = IterativeFibonacci.fib(num);  //saves number
            Profiler.end("IterativeFibonacciInt"); //ends method

            System.out.println("(Iterative/int) The " + num + "th Fibonacci number = " + iterativeFib + "."); //print statement

        }

        if( chosen.equals ("iterative") && secondArg.equals("long") ) // argument that accepts iterative & long

        {

            Profiler.begin("IterativeFibonacciLong"); //begins program for iterative fibLong
            long iterativeFibLong = IterativeFibonacci.fibLong(num); //saves value
            Profiler.end("IterativeFibonacciLong"); //ends method

            System.out.println("(Iterative/long) The " + num + "th Fibonacci number = " + iterativeFibLong + "."); //print statement

        }

        if( chosen.equals ("recursive") && secondArg.equals("int") ) // argument that accepts recursive & int

        {

            Profiler.begin("RecursiveFibonacciInt"); //begins method for recursive fib
            int recursiveFib = RecursiveFibonacci.fib(num); //saves value
            Profiler.end("RecursiveFibonacciInt"); //end method

            System.out.println("(Recursive/int) The " + num + "th Fibonacci number = " + recursiveFib + "."); //print statement

        }

        if( chosen.equals ("recursive ") && secondArg.equals("long") ) // argument that accepts recursive & long

        {

            Profiler.begin("RecursiveFibonacciLong"); //begins method for recursive fiblong
            long recursiveFibLong = RecursiveFibonacci.fibLong(num); //saves value
            Profiler.end("RecursiveFibonacciLong");

            System.out.println("(Recursive/long) The " + num + "th Fibonacci number = " + recursiveFibLong + "."); //print statement

        }

        if( chosen.equals ("all") ) // argument that accepts all which means both iterative and recursive
        {
            if( secondArg.equals("int") ) // argument only accepts int
            {
                //runs method RecursiveFibonacciInt and saves/prints value
                Profiler.begin("RecursiveFibonacciInt");
                int recursiveFib = RecursiveFibonacci.fib(num);
                Profiler.end("RecursiveFibonacciInt");

                System.out.println("(Recursive/int) The " + num + "th Fibonacci number = " + recursiveFib + ".");

                //runs method IterativeFibonacciInt and saves/prints value
                Profiler.begin("IterativeFibonacciInt");
                int iterativeFib = IterativeFibonacci.fib(num);
                Profiler.end("IterativeFibonacciInt");

                System.out.println("(Iterative/int) The " + num + "th Fibonacci number = " + iterativeFib + ".");

            }
            if( secondArg.equals("long") ) //argument only accepts long
            {
                Profiler.begin("RecursiveFibonacciLong");
                long recursiveFibLong = RecursiveFibonacci.fibLong(num);
                Profiler.end("RecursiveFibonacciLong");

                System.out.println("(Recursive/long) The " + num + "th Fibonacci number = " + recursiveFibLong + ".");

                //runs method IterativeFibonacciLong saves/prints value
                Profiler.begin("IterativeFibonacciLong");
                long iterativeFibLong = IterativeFibonacci.fibLong(num);
                Profiler.end("IterativeFibonacciLong");

                System.out.println("(Iterative/long) The " + num + "th Fibonacci number = " + iterativeFibLong + ".");

            }
        }

        if( chosen.equals ("recursive") && secondArg.equals("all")) //argument accepts recursive and all-- will do recursive and both int & long
        {
            //runs method RecursiveFibonacciInt and saves/prints value
            Profiler.begin("RecursiveFibonacciInt");
            int recursiveFib = RecursiveFibonacci.fib(num);
            Profiler.end("RecursiveFibonacciInt");

            System.out.println("(Recursive/int) The " + num + "th Fibonacci number = " + recursiveFib + ".");

            //runs method RecursiveFibonacciLong and saves/prints value
            Profiler.begin("RecursiveFibonacciLong");
            long recursiveFibLong = RecursiveFibonacci.fibLong(num);
            Profiler.end("RecursiveFibonacciLong");

            System.out.println("(Recursive/long) The " + num + "th Fibonacci number = " + recursiveFibLong + ".");

        }

        if( chosen.equals ("iterative") && secondArg.equals("all")) //argument accepts iterative and all
        {
            //runs method IterativeFibonacciInt and saves/prints value
            Profiler.begin("IterativeFibonacciInt");
            int iterativeFib = IterativeFibonacci.fib(num);
            Profiler.end("IterativeFibonacciInt");

            System.out.println("(Iterative/int) The " + num + "th Fibonacci number = " + iterativeFib + ".");

            //runs method IterativeFibonacciLong and saves/prints value
            Profiler.begin("IterativeFibonacciLong");
            long iterativeFibLong = IterativeFibonacci.fibLong(num);
            Profiler.end("IterativeFibonacciLong");

            System.out.println("(Iterative/long) The " + num + "th Fibonacci number = " + iterativeFibLong + ".");

        }

        if(chosen.equals ("all") && secondArg.equals("all")) //argument accepts all -- will execute both recursive & iterative and int & long
        {
             Profiler.begin("IterativeFibonacciInt");
            int iterativeFib = IterativeFibonacci.fib(num);
            Profiler.end("IterativeFibonacciInt");

            System.out.println("(Iterative/int) The " + num + "th Fibonacci number = " + iterativeFib + ".");

            Profiler.begin("IterativeFibonacciLong");
            long iterativeFibLong = IterativeFibonacci.fibLong(num);
            Profiler.end("IterativeFibonacciLong");

            System.out.println("(Iterative/long) The " + num + "th Fibonacci number = " + iterativeFibLong + ".");

            Profiler.begin("RecursiveFibonacciInt");
            int recursiveFib = RecursiveFibonacci.fib(num);
            Profiler.end("RecursiveFibonacciInt");

            System.out.println("(Recursive/int) The " + num + "th Fibonacci number = " + recursiveFib + ".");

            Profiler.begin("RecursiveFibonacciLong");
            long recursiveFibLong = RecursiveFibonacci.fibLong(num);
            Profiler.end("RecursiveFibonacciLong");

            System.out.println("(Recursive/long) The " + num + "th Fibonacci number = " + recursiveFibLong + ".");

        }

        System.out.println(); Profiler.print(new PrintWriter(System.out)); // prints time each method takes

        System.out.println("... End experiment with different Fibonacci " +
                "implementations"); //print statement

    }

}
