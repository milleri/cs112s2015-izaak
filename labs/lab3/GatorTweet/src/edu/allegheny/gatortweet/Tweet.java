package edu.allegheny.gatortweet;

import java.util.Date;
import java.util.Scanner;
import java.util.Iterator;

/**
 *@author Izaak Miller <milleri@allegheny.edu>
 *@since 2014-29-1
 */
public class Tweet
{

    private Date currentDate; /**current date*/

    private String message; /**message or tweet*/

    private static final int MAX_LENGTH = 140; /**max length that a tweet can be*/

    public Tweet()
    {
        currentDate = new Date();

        /** The tweet will have an instance of the date so it will say when it was sent*/
    }

    public boolean isValidMessage(String message)
    {
        if (message.length() <= 140 && message.length()>0)
            return true;
        else
            return false;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String toString()
    {
        return "(" + currentDate.toString() + ", " + message + ")";
    }

}
