package edu.allegheny.gatortweet;

import java.util.Scanner;
import java.util.ArrayList;
import java.io.*;
import java.io.FileNotFoundException;


/**
 * @author Izaak Miller <milleri@allegheny.edu>
 * @since @1-29-2014
 */

public class GatorTweet
{
    public static void main(String[] args)

     {
            File TweetList = new File ("tweet/Tweets.txt"); /** Makes the tweet.txt and object */
            ArrayList<Tweet> ValidTweets = new ArrayList<Tweet>(); /** Creates array list for valid tweets*/
            ArrayList<String> InvalidTweets = new ArrayList<String>(); /** Creates array list for invalid tweets*/

            try /** Try catch instead of throws; Have to catch the FileNotFoundException error */
            {
             Scanner scan = new Scanner(TweetList); /** Reading throw text file where tweets are */

                while (scan.hasNextLine()) /** Loop reads text file until there are no more tweets */
            {
                String newTweet = scan.nextLine();
                Tweet tw = new Tweet(); /** Creates an instance of the tweet */


                if (tw.isValidMessage(newTweet) == true) /** Read text file and calls method to see if the tweet is valid */
                {
                    tw.setMessage(newTweet); /** Sets the message */
                    ValidTweets.add(tw); /** Adds valid tweet to the corresponding array list */
                }
                else
                {
                    tw.setMessage(newTweet);
                    InvalidTweets.add(newTweet); /** Adds invalid tweet to the corresponding array list */
                }

            }
            System.out.println("Valid Tweets:");
           // for (Tweet outputTweet : ValidTweets)
           /** Tried to incorporate for each loop but it outputted the array list too many times */
                System.out.println(ValidTweets); /** Prints out the tweets stored in the array list */

            System.out.println("\nInvalid Tweets:");
           // for (String outputTweet : InvalidTweets)
                System.out.println(InvalidTweets);
            }catch (FileNotFoundException e)
            {
                System.out.println("An error has occurred");
            }

            System.out.println("Finished");
    }

}
