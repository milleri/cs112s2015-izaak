package edu.allegheny.experiment;

import java.util.LinkedList;

public class LinkedListBenchmarks {


  /**@Izaak Miller*/

  /** Predetermined sizes in case user doesn't want
   * to decide the size of the variables*/

  static int initialListSize = 1000000;
  static int addToFrontNumber = 1000;
  static int RemoveNumber = 1000;

  public static void main(String[] args) {
    LinkedList<Integer> linkedList = new LinkedList<Integer>(); /**Creating new LinkedList*/

    /**Allows for user to decide the intiaListSize, addToFrontNumber and RemoveNumber*/
    if (args.length > 0)
    {
        initialListSize = Integer.parseInt(args[0]);
    }
    if (args.length > 1)
    {
        addToFrontNumber = Integer.parseInt(args[1]);
    }
    if (args.length > 2)
    {
        RemoveNumber = Integer.parseInt(args[2]);
    }

    /**Timer for filling LinkedList*/
    long beforeFillLinkedList = System.currentTimeMillis();
    for(int i = 0; i<initialListSize; i++) {
      linkedList.add(i); /**Filling LinkedList determined by initialListSize*/
    }
    long afterFillLinkedList = System.currentTimeMillis();

    System.out.println("LinkedList size = " + linkedList.size());
    System.out.println("Fill LinkedList elapsed time = " + (afterFillLinkedList - beforeFillLinkedList));
    System.out.println("Fill LinkedList average time = " + (double)(afterFillLinkedList - beforeFillLinkedList) / (double)initialListSize);

    /**Timer for adding to LinkedList*/
    long beforeLinkedListFrontAdd = System.currentTimeMillis();
    for(int i = 0; i<addToFrontNumber; i++){
      linkedList.add(i, i); /**Adding to LinkedList based on addToFrontNumber*/
    }
    long afterLinkedListFrontAdd = System.currentTimeMillis();

    System.out.println("Add to LinkedList elapsed time = " + (afterLinkedListFrontAdd - beforeLinkedListFrontAdd));
    System.out.println("Add to LinkedList average time = " + (double)(afterLinkedListFrontAdd - beforeLinkedListFrontAdd) / (double)addToFrontNumber);

    /**Timer for removing from LinkedList*/
    long beforeLinkedListRemove = System.currentTimeMillis();
    for(int i = 0; i<RemoveNumber; i++){
      linkedList.remove(i); /**Removing from LinkedList determined by RemoveNumber*/
    }
    long afterLinkedListRemove = System.currentTimeMillis();

    System.out.println("Remove from LinkedList elapsed time = " + (afterLinkedListRemove - beforeLinkedListRemove));
    System.out.println("Remove from LinkedList average time = " + (double)(afterLinkedListRemove - beforeLinkedListRemove) / (double)RemoveNumber);

  }
}
