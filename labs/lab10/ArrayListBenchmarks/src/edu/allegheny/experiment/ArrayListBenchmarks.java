package edu.allegheny.experiment;

import java.util.ArrayList;

public class ArrayListBenchmarks {

    /**@Izaak Miller*/

  /** Predetermined sizes in case user doesn't want
   * to decide the size of the variables*/
  static int initialListSize = 1000000;
  static int addToFrontNumber = 1000;
  static int RemoveNumber = 1000;

  public static void main(String[] args) {
    ArrayList<Integer> arrayList = new ArrayList<Integer>(); /**Creating new ArrayList*/

    /**Allows for user to decide the intiaListSize, addToFrontNumber and RemoveNumber*/
    if (args.length > 0)
    {
        initialListSize = Integer.parseInt(args[0]);
    }
    if (args.length > 1)
    {
        addToFrontNumber = Integer.parseInt(args[1]);
    }
    if (args.length > 2)
    {
        RemoveNumber = Integer.parseInt(args[2]);
    }

    /**Timer for filling ArrayList*/
    long beforeFillArrayList = System.currentTimeMillis();
    for(int i = 0; i<initialListSize; i++) {
      arrayList.add(i); /**Filling ArrayList based on initialListSize*/
    }
    long afterFillArrayList = System.currentTimeMillis();

    System.out.println("ArrayList size = " + arrayList.size());
    System.out.println("Fill ArrayList elapsed time = " + (afterFillArrayList - beforeFillArrayList));
    System.out.println("Fill ArrayList average time = " + (double)(afterFillArrayList - beforeFillArrayList) / (double)initialListSize);

    /**Timer for adding to ArrayList*/
    long beforeArrayListFrontAdd = System.currentTimeMillis();
    for(int i = 0; i<addToFrontNumber; i++){
      arrayList.add(i, i); /**Adding at position i for a certain number of times (addToFrontNumber)*/
    }
    long afterArrayListFrontAdd = System.currentTimeMillis();

    System.out.println("Add to ArrayList elapsed time = " + (afterArrayListFrontAdd - beforeArrayListFrontAdd));
    System.out.println("Add to ArrayList average time = " + (double)(afterArrayListFrontAdd - beforeArrayListFrontAdd) / (double)addToFrontNumber);

    /**Timer for removing from ArrayList*/
    long beforeArrayListRemove = System.currentTimeMillis();
    for(int i = 0; i<RemoveNumber; i++){
      arrayList.remove(i); /**Removing from ArrayList for a certain number of times (RemoveNumber)*/
    }
    long afterArrayListRemove = System.currentTimeMillis();

    System.out.println("Remove from ArrayList elapsed time = " + (afterArrayListRemove - beforeArrayListRemove));
    System.out.println("Remove from ArrayList average time = " + (double)(afterArrayListRemove - beforeArrayListRemove) / (double)RemoveNumber);


  }
}
