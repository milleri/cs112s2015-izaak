package edu.allegheny.experiment;

import java.util.Vector;

public class VectorBenchmarks {

/**@Izaak Miller*/

  /** Predetermined sizes in case user doesn't want
  * to decide the size of the variables*/
  static int initialListSize = 1000000;
  static int addToFrontNumber = 1000;
  static int RemoveNumber = 1000;

  public static void main(String[] args) {
    Vector<Integer> vector = new Vector<Integer>(); /**Creating a new Vector*/

    /**Allows for user to decide the intiaListSize, addToFrontNumber and RemoveNumber*/
    if (args.length > 0)
    {
        initialListSize = Integer.parseInt(args[0]);
    }
    if (args.length > 1)
    {
        addToFrontNumber = Integer.parseInt(args[1]);
    }
    if (args.length > 2)
    {
        RemoveNumber = Integer.parseInt(args[2]);
    }

    /**Timer for filling Vector*/
    long beforeFillVector = System.currentTimeMillis();
    for(int i = 0; i<initialListSize; i++) {
      vector.add(i); /**Filling ArrayList based on initialListSize*/
    }
    long afterFillVector = System.currentTimeMillis();

    System.out.println("Vector size = " + vector.size());
    System.out.println("Fill Vector elapsed time = " + (afterFillVector - beforeFillVector));
    System.out.println("Fill Vector average time = " + (double)(afterFillVector - beforeFillVector) / (double)initialListSize);

    /**Timer for adding to Vector*/
    long beforeVectorFrontAdd = System.currentTimeMillis();
    for(int i = 0; i<addToFrontNumber; i++){
      vector.add(i, i); /**Adding to Vector size determined by addToFrontNumber*/
    }
    long afterVectorFrontAdd = System.currentTimeMillis();

    System.out.println("Add to Vector elapsed time = " + (afterVectorFrontAdd - beforeVectorFrontAdd));
    System.out.println("Add to Vector average time = " + (double)(afterVectorFrontAdd - beforeVectorFrontAdd) / (double)addToFrontNumber);

    /**Timer for removing from Vector*/
    long beforeVectorRemove = System.currentTimeMillis();
    for(int i = 0; i<RemoveNumber; i++){
      vector.remove(i); /**Removing from Vector determined by RemoveNumber*/
    }
    long afterVectorRemove = System.currentTimeMillis();

    System.out.println("Remove from Vector elapsed time = " + (afterVectorRemove - beforeVectorRemove));
    System.out.println("Remove from Vector average time = " + (double)(afterVectorRemove - beforeVectorRemove) / (double)RemoveNumber);

  }
}
